# Scripts

## Encode / Decode levels

```shell
yarn # Install deps

yarn start decode-level levels/532.xml # Generate a levels/532.json

yarn start encode-level levels/532.json # Build back the levels/532.xml
```

# Documentation

## FlashVars

- `swf`: play.swf SWF.
- `$try`: Unused.
- `$lang`: l18n, supported, but all string are in french. Can be "en", "fr", "es". Defaults to "en".
- `$loadUrl`: URL to load the XML level.
- `$endUrl`: endpoint to send the [exit request](#Exit-request-format)
- `$help`: if `$help` is `1`, display some infos in the pause menu, else hide them.
- `$mode`: Must match mode in level XML.
- `$saveUrl`: Used for the level editor. RIP
- `$saveAct`: Used for the level editor. RIP

Misc variables :

- `data`: if `data` is `2`, the `swf` come from `data.pioupiouz.com`. Useless with [loader modifications](#Loader-SWF-modifications).
- `vs`: if `cs` is `1`, the `swf` come from `dev.pioupiouz.com`. Useless with [loader modifications](#Loader-SWF-modifications).
- `redirect`: do not play the game, just send a POST request with `mid` (machine id) to the given url.

### Exit request format

- `$time`: elapsed time
- `$piouz`: saved piouz
- `$pid`: id given to swf
- `$theme`: did
- `$soluce`: the replay

## Levels

### XML

Main node can have any name

Attributes :

- `piouz`: Piouz goal
- `mode`:
  - 0 => Replay mode (must have a soluce node)
  - 1 => Normal mode, and save history
  - 2 => Preview mode
- `time`: Max time ? (seems unused)
- `title`: level title (seems inutilisé)
- `status`: Only in `mode` 2, display a medal
  - 0: None
  - 1: Dead
  - 2: Iron
  - 3: Gold
  - 4+: Star
- `id`: level id
- `playId`: level id (override `id` in exit request)

Children:

- `tuto` node: string displayed in the game, for the tuto
- `cards` node: seems unused
- `soluce` node: replay mode
- `data` node: see bellow

### Level data

Encoded with `PersistCodec` serialization (`mtbon` package in eternalfest).

- `1*gU6` - `tiles`: Array
  - `x`
  - `y`
  - `*B` - `id`
- `7=joK` - `platforms`: Array
  - `x`
  - `y`
  - ` (` - `w`: width of platform
  - `9*I` - `rid`
  - `9;I` - `rot`: rotation in deg
- `{)59` - `piou`: [ x, y, direction ] => direction can be -1 (left) or 1 (right)
- `;xD` - `out`: [ [ x, y ], ... ] => position of goals
- `;8,PR` - `action`: [ [ id, number ], ... ] => number of piouz actions available
- `9cgEI(` - `caisse`: [ [ x, y, id, number ], ... ] => position with the `action` it gives.
- `size`: [ width, height ]
- `49-` - `did`: theme

# Loader SWF modifications

There is Codec encoded variables to check if the host starts with "www.pioupiouz.com" or "data.pioupiouz.com" (or "dev.pioupiouz.com").
I just change theses variables with an empty string.
