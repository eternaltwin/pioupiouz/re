import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import {
  X2jOptions,
  XMLBuilder,
  XMLParser,
  XmlBuilderOptions,
} from "fast-xml-parser";
import { readFileSync, writeFileSync } from "fs";
import { apply_mapping, unapply_mapping } from "./obfu_mappings";
import { parse as mtBonParse, emit as mtBonEmit } from "@eternalfest/mtbon";

const XMLOptions: Partial<X2jOptions> & Partial<XmlBuilderOptions> = {
  ignoreAttributes: false,
  attributeNamePrefix: "@_",
  format: true,
};

const decodeLevel = (xml: string, outJson: string | undefined) => {
  outJson = outJson || xml.substring(0, xml.lastIndexOf(".")) + ".json";

  const file = readFileSync(xml);
  const xmlData = new XMLParser(XMLOptions).parse(file);

  for (let key in xmlData) {
    if (xmlData[key]?.["data"]) {
      const data = mtBonParse(xmlData[key]["data"]);
      apply_mapping(data);
      xmlData[key]["data"] = data;
    }
  }

  const jsonData = JSON.stringify(xmlData, null, 2);
  writeFileSync(outJson, jsonData);
};

const encodeLevel = (json: string, outXml: string | undefined) => {
  outXml = outXml || json.substring(0, json.lastIndexOf(".")) + ".xml";

  const file = readFileSync(json);
  const jsonData = JSON.parse(file.toString());

  for (let key in jsonData) {
    if (jsonData[key]?.["data"]) {
      unapply_mapping(jsonData[key]["data"]);
      const data = mtBonEmit(jsonData[key]["data"]);
      jsonData[key]["data"] = data;
    }
  }

  const xmlData = new XMLBuilder(XMLOptions).build(jsonData);
  writeFileSync(outXml, xmlData.toString());
};

yargs(hideBin(process.argv))
  .command(
    "decode-level <xml> [--out <json>]",
    "parse the XML level to JSON",
    (yargs) => {
      return yargs
        .positional("xml", {
          describe: "the XML of the level",
          type: "string",
          demandOption: true,
        })
        .option("out", {
          describe: "the output JSON file (default: <xml>.json)",
          type: "string",
        });
    },
    (argv) => {
      decodeLevel(argv.xml, argv.out);
    }
  )
  .command(
    "encode-level <json> [--out <xml>]",
    "encode the JSON level to XML",
    (yargs) => {
      return yargs
        .positional("json", {
          describe: "the JSON of the level",
          type: "string",
          demandOption: true,
        })
        .option("out", {
          describe: "the output XML file (default to <json>.xml)",
          type: "string",
        });
    },
    (argv) => {
      encodeLevel(argv.json, argv.out);
    }
  )
  .command(
    "display-mtbon <value>",
    "Just display PersistantCodec data",
    (yargs) => {
      return yargs.positional("value", {
        describe: "the encoded value",
        type: "string",
        demandOption: true,
      });
    },
    (argv) => {
      console.log(JSON.stringify(mtBonParse(argv.value), null, 2));
    }
  )
  .demandCommand(1)
  .parse();
